#!/bin/bash
#------------------------------------------------------------------------------------------------------------------------------
#
# Descrição do script: Instalação completa e automática do NEMON
# Data inicial: 07/06/2019
# Data final: 30/07/2019
#
#------------------------------------------------------------------------------------------------------------------------------
# Autor: Ayran Louro, <alouro@nebrasil.com.br>
# Autor: Marcel Moreira, <mmoreira@nebrasil.com.br>
# Versão: 1.0 - inicio
# Versão: 2.0 - Ajustes de pacotes.
#------------------------------------------------------------------------------------------------------------------------------

ping 8.8.8.8 -c3
out=$?

if [ $out -ne 0 ]; then
	data=$(date)
	echo "${data} | Network is down" >> /tmp/nemon_erros.txt
	exit
else
	data=$(date)
	echo "${data} | Network is up" >> /tmp/nemon_erros.txt
fi

### Desabilitando Selinux
cd /tmp
sed -i 's/SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
setenforce 0

### Instalando pacotes e download do Nagios
yum install -y gcc glibc glibc-common wget unzip httpd php gd gd-devel perl postfix curl
#wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.3.tar.gz --no-check-certificate
nagcorver=$(curl --silent  https://github.com/NagiosEnterprises/nagioscore/releases/latest/download | grep -o "/download/nagios-[0-9\.]*") 
wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/releases/latest/download/${nagcorver##*/}.tar.gz --no-check-certificate 
tar xzf nagioscore.tar.gz

### Compilando e instalando binários
cd /tmp/nagios-*/
./configure >> /tmp/nemon_install.log

### Criando arquivos de configuração e grupo de usuários
make all
make install-groups-users
usermod -a -G nagios apache
usermod -a -G apache nagios
make install
make install-daemoninit
systemctl enable httpd.service
make install-commandmode
make install-webconf
make install-config

### Regras de firewall
firewall-cmd --zone=public --add-port=80/tcp
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=5666/tcp
firewall-cmd --zone=public --add-port=5666/tcp --permanent

### Criar senha e iniciar o serviço
htpasswd -b -c /usr/local/nagios/etc/htpasswd.users nagiosadmin nagios
systemctl start httpd
systemctl start nagios.service

### Instalando Nagios Plugins
yum install -y gcc glibc glibc-common make gettext automake autoconf wget openssl-devel net-snmp net-snmp-utils epel-release
yum install -y perl-Net-SNMP
cd /tmp
wget -O nagios-plugins.tar.gz https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/nagios-plugins-release-2.2.1.tar.gz --no-check-certificate
tar zxf nagios-plugins.tar.gz
cd /tmp/nagios-plugins-release-2.2.1/
./tools/setup
./configure
make
make install
systemctl restart nagios
unset out

### Instalação do Grafico(pnp4) Nagios Core
yum install -y gcc-c++ rrdtool perl-Time-HiRes perl-rrdtool
cd /usr/src/
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/pnp4nagios-0.6.6.tar.gz --no-check-certificate
tar zxvf pnp4nagios-0.6.6.tar.gz
cd pnp4nagios-0.6.6
./configure
make all
make install
make install-config
make install-webconf
ldconfig
cd /etc/httpd/conf.modules.d/
ln -s ../mods-available/rewrite.load rewrite.load
cd /tmp
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/nagios.cfg --no-check-certificate
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/commands.cfg --no-check-certificate
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/templates.cfg --no-check-certificate
mv -f commands.cfg /usr/local/nagios/etc/objects/
mv -f nagios.cfg /usr/local/nagios/etc/
mv -f templates.cfg /usr/local/nagios/etc/objects/ 
cd /usr/local/pnp4nagios/etc/
mv process_perfdata.cfg-sample process_perfdata.cfg
mv npcd.cfg-sample npcd.cfg
service httpd restart
cd /usr/local/pnp4nagios/share/
mv install.php install.php-old
/usr/local/pnp4nagios/bin/npcd -d -f /usr/local/pnp4nagios/etc/npcd.cfg

### Instalar Tema do Nagios Core 
yum install unzip -y
cd /usr/local/nagios/ 
tar czf nagios-share.tar.gz share/
cd /usr/src/
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/arana_style-1.0for-Nagios3x-ENG.zip --no-check-certificate
unzip /usr/src/arana_style-1.0for-Nagios3x-ENG.zip
cd arana_style-1.0for-Nagios3x-ENG
cd arana_style/
Yes | cp -pR * /usr/local/nagios/share/
systemctl restart httpd
systemctl restart nagios

### Instalar MARIADB
sudo yum install wget
yum install -y mariadb-server mariadb-devel mariadb
systemctl enable mariadb.service
systemctl start mariadb.service
/usr/bin/mysqladmin -u root password 'nagios'

### Atualizar a versão do PHP
yum install -y epel-release
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum makecache fast
yum remove -y php-common php php-gd php-cli
yum install -y php55w-common php55w php55w-gd php55w-cli php55w-mysql php55w-devel php55w-pear
yum install -y libssh2-devel
pecl channel-update pecl.php.net
pecl install ssh2
echo "date.timezone='America/Sao_Paulo'" >> /etc/php.ini
echo "extension=ssh2.so" >> /etc/php.ini
systemctl restart httpd
cd /tmp
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/nagiosql-3.4.0.tar.gz --no-check-certificate
tar xvf nagiosql-3.4.0.tar.gz
mv nagiosql-3.4.0 /var/www/html/
mv /var/www/html/nagiosql-3.4.0/ /var/www/html/nagiosql
restorecon -Rv /var/www/html/nagiosql/
mkdir -p /usr/local/nagios/etc/nagiosql
chown nagios:apache /usr/local/nagios/etc/nagiosql/
chmod o+w /var/www/html/nagiosql/config
cd /tmp 
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/nagiosql-dir.tgz --no-check-certificate
tar xvf nagiosql-dir.tgz
mv -f nagiosql /var/www/html/
systemctl restart nagios
echo "CREATE DATABASE nagios DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -uroot -pnagios
echo "CREATE USER 'nagios'@'localhost' IDENTIFIED BY 'nagios';" | mysql -uroot -pnagios
echo "GRANT USAGE ON *.* TO 'nagios'@'localhost' IDENTIFIED BY 'nagios' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;" | mysql -uroot -pnagios
echo "GRANT ALL PRIVILEGES ON nagios.* TO 'nagios'@'localhost' WITH GRANT OPTION;" | mysql -uroot -pnagios
cp /etc/sysctl.conf /etc/sysctl.conf_backup
sed -i '/msgmnb/d' /etc/sysctl.conf
sed -i '/msgmax/d' /etc/sysctl.conf
sed -i '/shmmax/d' /etc/sysctl.conf
sed -i '/shmall/d' /etc/sysctl.conf
printf "\n\nkernel.msgmnb = 131072000\n" >> /etc/sysctl.conf
printf "kernel.msgmax = 131072000\n" >> /etc/sysctl.conf
printf "kernel.shmmax = 4294967295\n" >> /etc/sysctl.conf
printf "kernel.shmall = 268435456\n" >> /etc/sysctl.conf
sysctl -e -p /etc/sysctl.conf
cd /tmp
wget -O ndoutils.tar.gz https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/ndoutils-2.1.3.tar.gz --no-check-certificate
tar xzf ndoutils.tar.gz
cd /tmp/ndoutils-2.1.3/
./configure
make all
make install
cd db/
./installdb -u 'nagios' -p 'nagios' -h 'localhost' -d nagios
cd ..
make install-config
mv /usr/local/nagios/etc/ndo2db.cfg-sample /usr/local/nagios/etc/ndo2db.cfg
sed -i 's/^db_user=.*/db_user=nagios/g' /usr/local/nagios/etc/ndo2db.cfg
sed -i 's/^db_pass=.*/db_pass=nagios/g' /usr/local/nagios/etc/ndo2db.cfg
mv /usr/local/nagios/etc/ndomod.cfg-sample /usr/local/nagios/etc/ndomod.cfg
make install-init
systemctl enable ndo2db.service
systemctl start ndo2db.service
#printf "\n\n# NDOUtils Broker Module\n" >> /usr/local/nagios/etc/nagios.cfg
#printf "broker_module=/usr/local/nagios/bin/ndomod.o config_file=/usr/local/nagios/etc/ndomod.cfg\n" >> /usr/local/nagios/etc/nagios.cfg
systemctl restart nagios.service
systemctl status nagios.service
cd /tmp
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/nagvis-1.9.5.tar.gz --no-check-certificate
tar zxvf nagvis-1.9.5.tar.gz
cd nagvis-1.9.5
yum install rsync graphviz -y
chmod 777 install.sh
./install.sh -q
mkdir -p /usr/local/nagios/etc/nagiosql/backup/services
mkdir -p /usr/local/nagios/etc/nagiosql/backup/hosts
chmod -R 775 /usr/local/nagios
chown -R nagios.apache /usr/local/nagios
cd /tmp
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/nagvis.ini.php --no-check-certificate
mv -f /tmp/nagvis.ini.php /usr/local/nagvis/etc/nagvis.ini.php
cd /tmp
wget https://gitlab.com/nemon-publico/arquivos-nemon-public/raw/master/linux-nrpe-agent.tar.gz --no-check-certificate
tar xzf linux-nrpe-agent.tar.gz
cd linux-nrpe-agent
./fullinstall -n
systemctl restart httpd
systemctl restart ndo2db
systemctl restart nagios
